package com.tdd;

import lombok.AllArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class TDDLoginServiceTest {

    private Login login;
    private Encryption encryption;

    @BeforeEach
    public void runAll() {
        login = new LoginImp();
    }

    @ParameterizedTest
    @CsvSource({"Lilly,starwars1", "Alex,starwars12", "Sara,starwars123", "Steve,starwars1234", "Nick,starwars125"})
    public void shouldSucceedIfExistingUser (String name, String pass) {
        String password = encryption.encrypt.apply(pass);
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(login.hasUser(name,password).get().getToken());
    }

    @ParameterizedTest
    @CsvSource({"Lilly,pass", "Al,starwars12", "Sara,nowars123", "Steve,134", "Nick,no"})
    public void shouldFailIfNotFound (String name, String pass) {
        String password = encryption.encrypt.apply("majd");
        assertEquals(Optional.empty(),login.hasUser("anna",password));
    }

    @ParameterizedTest
    @CsvSource( {"Lilly,", ",starwars12", ",", ",starwars1234", ",starwars125"} )
    public void shouldFailOnNull (String name, String pass) {
        assertThrows(NullPointerException.class, () ->
            login.hasUser(name, pass));
    }

    @Test
    void if_account_premission_read(){
        Token token = mock(Token.class);
        List<Right> permissions = login.getPermissionsByResource(token, "ACCOUNT");


    }
    @Test
    void if_account_premission_read_write(){
        Token token = mock(Token.class);
        List<Right> permissions = login.getPermissionsByResource(token, "ACCOUNT");
        assertTrue(permissions.contains(Right.READ));
        assertTrue(permissions.contains(Right.WRITE));
    }

    @Test
    void if_account_premission_EXECUTE(){
        Token token = mock(Token.class);
        List<Right> permissions = login.getPermissionsByResource(token, "PROVISION_CALC");
        assertTrue(permissions.contains(Right.EXECUTE));
    }
    @Test
    void if_account_premission_not_found(){
        Token token = mock(Token.class);
        List<Right> permissions = login.getPermissionsByResource(token, "manager");
        assertTrue(permissions.contains(Right.NORIGHTSFOUND));
    }
}
