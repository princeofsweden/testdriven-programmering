package com.tdd;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class EncryptionTest {

    private static final String DATA = "Hello World";
    private static final String DATA_ENCRP = "Idmkp\u001FXnske";

    @BeforeEach
    void build_constructor(){

    }
    @Test
    void encrypt_if_right() {
        String encrypt = Encryption.encrypt.apply(DATA);
        assertEquals(DATA_ENCRP,encrypt);
    }

    @Test
    void encrypt_if_false() {
        String encrypt = Encryption.encrypt.apply(DATA);
        assertNotEquals("Idmkp\u001FXnskf", encrypt);
    }
}