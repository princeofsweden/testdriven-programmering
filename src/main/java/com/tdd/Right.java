package com.tdd;

public enum Right {
    READ,
    WRITE,
    EXECUTE,
    NORIGHTSFOUND;
}
