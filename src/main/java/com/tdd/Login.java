package com.tdd;

import java.util.List;
import java.util.Optional;

public interface Login {
    Optional<Token> hasUser(String name, String pass);

    List<Right> getPermissionsByResource(Token token, String account);
}
