package com.tdd;

import java.util.*;

public class LoginImp implements Login {

    HashMap<Token,User> tokenHashMap =  new HashMap<>();
    private Encryption encryption;

    @Override
    public Optional<Token> hasUser(String user, String password) {
        if (user == null || password == null) {
            throw new NullPointerException("Name or pass cannot be null!!!");
        } else {
            return getListOfUsers().stream().filter(u ->
                    (u.getName().equals(user) && u.getPassword().equals(password))).findFirst()
                    .map(this::createToken);
        }
    }

    @Override
    public List<Right> getPermissionsByResource(Token token, String account) {
        List<Right> prem = new ArrayList<>();
        if (account.equals("ACCOUNT")){
            prem.add(Right.READ);
            prem.add(Right.WRITE);
        } else if (account.equals("PROVISION_CALC"))
            prem.add(Right.EXECUTE);
        else prem.add(Right.NORIGHTSFOUND);
        return prem;
    }

    private Token createToken(User user) {
        Token token = new Token(UUID.randomUUID().toString());
        tokenHashMap.put(token,user);
        return token;
    }

    private List<User> getListOfUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User("Lilly", Encryption.encrypt.apply("starwars1")));
        users.add(new User("Alex", Encryption.encrypt.apply("starwars12")));
        users.add(new User("Sara", Encryption.encrypt.apply("starwars123")));
        users.add(new User("Steve", Encryption.encrypt.apply("starwars1234")));
        users.add(new User("Nick", Encryption.encrypt.apply("starwars125")));
        return users;
    }
}
