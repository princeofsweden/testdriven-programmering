package com.tdd;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Value;

//@AllArgsConstructor
//@Getter

@Value
class Token {
    String token;
}