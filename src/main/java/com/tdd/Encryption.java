package com.tdd;

import java.util.function.Function;

public interface Encryption {
    Function<String,String> encrypt = data -> {
        byte[] byteData = data.getBytes();
        byte[] enc = new byte[byteData.length];
        for (int i = 0; i < byteData.length; i++)
        {
            enc[i] = (byte) ((i % 2 == 0) ? byteData[i] + 1 : byteData[i] - 1);
        }
        return new String(enc);
    };
}
