package com.tdd;

import lombok.Data;

import java.util.List;

@Data
public class User {
    private final String name;
    private final String password;
}
